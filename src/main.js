import Vue from 'vue';
import Vuelidate from 'vuelidate';
import VueTheMask from 'vue-the-mask';
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue';
import App from './App.vue';
import store from './store';

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

Vue.use(Vuelidate);

Vue.use(VueTheMask);

// Install BootstrapVue
Vue.use(BootstrapVue);
// Optionally install the BootstrapVue icon components plugin
Vue.use(BootstrapVueIcons);

Vue.config.productionTip = false;

new Vue({
  store,
  render: (h) => h(App),
}).$mount('#app');
