import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    isModalOpen: false,
    modifType: null,
    editIndex: null,
    isItemEmpty: true,
  },
  mutations: {
    modalOpen(state, payload) {
      // console.log('payload: ', payload);
      state.isModalOpen = true;
      state.modifType = payload.type;
      state.editIndex = payload.index;
    },
    modalClose(state) {
      state.isModalOpen = false;
      state.modifType = null;
      state.editIndex = null;
    },
    changeItemStatus(state, status) {
      state.isItemEmpty = status;
    },
  },
  actions: {
    openModal: ({ commit }, payload) => commit('modalOpen', payload),
    // openModal: ({ commit }, type, index) => commit('modalOpen', type, index),
    closeModal: ({ commit }) => commit('modalClose'),
    setItemEmpty: ({ commit }, payload) => commit('changeItemStatus', payload),
  },
  getters: {
    modalIsOpen: (state) => state.isModalOpen,
    modifType: (state) => state.modifType,
    editIndex: (state) => state.editIndex,
    itemIsEmpty: (state) => state.isItemEmpty,
  },
});
