# Explorance frontend development test with VueJS

## Requirements:

- Brand the form to Explorance
- Add a Birthday field
- Add #Items (integer) field
- Transform the Add/Edit section into a Pop-Over window
- Add a validation of the fields
- Add an empty state (no items at all)
  - state of items field?
    - of each user?
    - should it not be an attribute of user object? as of DoB?
  - or state of the user list?
    - no user / empty list -> empty state?

## Todos (v0.2.0)

- set focus to the 1st field (name) when modal open
- add delete/edit icons
  - Bootstrap Icons
    - https://icons.getbootstrap.com/icons/x/
    - https://icons.getbootstrap.com/icons/pencil/
- styling (branding)
  - buttons (size, color, padding,...)
  - font size
  - spacing
- improve validation
  - maxlength
  - dob: add calendar to choose from - overkill?
- update only if user infos differ
  - add id attribute
  - newobj === oldobj
  - with big data
    - costly check on client-side
    - real-life scenario: more efficient query with DB index
- add only if user non-existing
  - add id attribtute
  - check: fullname + dob
  - with big data
    - costly check on client-side
    - real-life scenario: more efficient query with DB index
- document/comment code
- add unit test

## Project Infos:
- GitLab for versioning
- GitLab CI tool to continuously deploy on AWS S3 bucket (see .gitlab-ci.yml)
- Repo: https://gitlab.com/chinh-dev11/test-explorance-vue
- Url: test-explorance-vue.chinhle.ca

___


## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
